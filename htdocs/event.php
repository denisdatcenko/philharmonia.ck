<?php 
    require_once ("database/connection.php");
    require_once ("model/announcements.php");
    
    $link = db_connect();
    
    if (isset($_GET['id']) & isset($_GET['source'])) {
        
        $id = $_GET['id'];
        $source = $_GET['source'].".php";

        $event = announcements_get($link, $id);
        include("views/event.php");
        
    }
?>