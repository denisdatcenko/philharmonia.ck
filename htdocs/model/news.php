<?php
    function news_all($link){

        $query = "SELECT * FROM news ORDER BY newsDate DESC, newsID DESC";
        $result = mysqli_query($link, $query);

        if (!$result)
            die(mysqli_error($link));

        $n = mysqli_num_rows($result);
        $news = array();

        for ($i=0; $i<$n; $i++)
        {
            $row = mysqli_fetch_assoc($result);
            $news[] = $row;
        }

        return $news;
    }

    function news_recent($link){

        $query = "SELECT * FROM news ORDER BY newsDate DESC, newsID DESC LIMIT 4";
        $result = mysqli_query($link, $query);

        if (!$result)
            die(mysqli_error($link));

        $n = mysqli_num_rows($result);
        $news = array();

        for ($i=0; $i<$n; $i++)
        {
            $row = mysqli_fetch_assoc($result);
            $news[] = $row;
        }

        return $news;
    }

    function news_newID($link){$query = "SELECT newsID FROM news ORDER BY newsID DESC LIMIT 1";
        $result = mysqli_query($link, $query);

        if (!$result)
            die(mysqli_error($link));

        $id = mysqli_fetch_assoc($result);
                                        
        return $id['newsID'];
        
    }

    function news_get($link, $id){
       $query = sprintf("SELECT * FROM news WHERE newsID=%d", (int)$id);
       $result = mysqli_query($link, $query);

       if (!$result)
           die (mysqli_error($link));

       $news = mysqli_fetch_assoc($result);

       return $news;
        }

    function news_new($link, $title, $date, $content, $preview, $photo){
        $title = trim($title);
        $content = trim($content);
        $preview = trim($preview);

        if ($title == '') 
            return false;

        $t = "INSERT INTO news (newsTitle, newsDate, newsContent, newsPreview) VALUES ('%s', '%s', '%s', '%s')";

        $query = sprintf($t, mysqli_real_escape_string($link, $title),
                             mysqli_real_escape_string($link, $date),
                             mysqli_real_escape_string($link, $content),
                             mysqli_real_escape_string($link, $preview));

        $result = mysqli_query($link, $query);

        if (!$result)
            die(mysqli_error($link));
        
        $id = news_newID($link);
        $path = '/img/news_' . $id . ".jpg";
        
        $t = "UPDATE news  SET newsPhotoPath = ('%s') WHERE newsID=('%s')";
        $query = sprintf($t, mysqli_real_escape_string($link, $path), (int)$id);
        $result = mysqli_query($link, $query);
        
        move_uploaded_file($photo['tmp_name'], '..'.$path);
        

        return true;

        }

    function news_edit($link, $id, $title, $date, $content, $preview, $photo){
        
        $title = trim($title);
        $content = trim($content);
        $preview = trim($preview);

        if ($title == '')
            return false;
        
        $t = "UPDATE news 
            SET newsTitle = ('%s'),
                newsDate = ('%s'), 
                newsContent = ('%s'), 
                newsPreview = ('%s') 
            WHERE newsID=('%s')";
        $query = sprintf($t, 
                         mysqli_real_escape_string($link, $title),  
                         mysqli_real_escape_string($link, $date), 
                         mysqli_real_escape_string($link, $content),
                         mysqli_real_escape_string($link, $preview),
                         (int)$id);
        $result = mysqli_query($link, $query);
        
        move_uploaded_file($photo['tmp_name'], '../img/news_' . $id . ".jpg");
        
        }

    function news_delete($link, $id){
        
        $path = $_SERVER['DOCUMENT_ROOT'].'img/news_' . $id . ".jpg";
        unlink($path);
        
        $query = sprintf("DELETE FROM news WHERE newsID=%d", (int)$id);
        $result = mysqli_query($link, $query);

        }


?>