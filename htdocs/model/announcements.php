<?php
    function announcements_all($link){

        $query = "SELECT * FROM announcements ORDER BY eventDate DESC";
        $result = mysqli_query($link, $query);

        if (!$result)
            die(mysqli_error($link));

        $n = mysqli_num_rows($result);
        $events = array();

        for ($i=0; $i<$n; $i++)
        {
            $row = mysqli_fetch_assoc($result);
            $events[] = $row;
        }

        return $events;
    }

    function announcements_upcoming($link){

        $query = sprintf("SELECT * FROM announcements WHERE eventDate>('%s') ORDER BY eventDate ASC LIMIT 3", date('Y-m-d H:i'));
        $result = mysqli_query($link, $query);

        if (!$result)
            die(mysqli_error($link));

        $n = mysqli_num_rows($result);
        $events = array();

        for ($i=0; $i<$n; $i++)
        {
            $row = mysqli_fetch_assoc($result);
            $events[] = $row;
        }

        return $events;
    }

    function announcements_newID($link){$query = "SELECT eventID FROM announcements ORDER BY eventID DESC LIMIT 1";
        $result = mysqli_query($link, $query);

        if (!$result)
            die(mysqli_error($link));

        $id = mysqli_fetch_assoc($result);
                                        
        return $id['eventID'];
        
    }

    function announcements_get($link, $id){
       $query = sprintf("SELECT * FROM announcements WHERE eventID=%d", (int)$id);
       $result = mysqli_query($link, $query);

       if (!$result)
           die (mysqli_error($link));

       $event = mysqli_fetch_assoc($result);

       return $event;
        }

    function announcements_new($link, $name, $date, $time, $description, $poster){
        $name = trim($name);
        $description = trim($description);
        $datetime=$date . ' ' . $time;

        if ($name == '')
            return false;

        $t = "INSERT INTO announcements (eventName, eventDate, eventDescription) VALUES ('%s', '%s', '%s')";

        $query = sprintf($t, mysqli_real_escape_string($link, $name),
                             mysqli_real_escape_string($link, $datetime),
                             mysqli_real_escape_string($link, $description));

        $result = mysqli_query($link, $query);

        if (!$result)
            die(mysqli_error($link));
        
        $id = announcements_newID($link);
        $path = '/img/ann_' . $id . ".jpg";
        
        $t = "UPDATE announcements  SET eventPosterPath = ('%s') WHERE eventID=('%s')";
        $query = sprintf($t, mysqli_real_escape_string($link, $path), (int)$id);
        $result = mysqli_query($link, $query);
        
        move_uploaded_file($poster['tmp_name'], '..'.$path);
        

        return true;

        }

    function announcements_edit($link, $id, $name, $date, $time, $description, $poster){
        
        $name = trim($name);
        $description = trim($description);
        $datetime=$date . ' ' . $time;

        if ($name == '')
            return false;
        
        $t = "UPDATE announcements 
            SET eventName = ('%s'),
                eventDate = ('%s'), 
                eventDescription = ('%s') 
            WHERE eventID=('%s')";
        $query = sprintf($t, 
                         mysqli_real_escape_string($link, $name),  
                         mysqli_real_escape_string($link, $datetime), 
                         mysqli_real_escape_string($link, $description),
                         (int)$id);
        $result = mysqli_query($link, $query);
        
        move_uploaded_file($poster['tmp_name'], '../img/ann_' . $id . ".jpg");
        
        }

    function announcements_delete($link, $id){
        
        $path = $_SERVER['DOCUMENT_ROOT'].'img/ann_' . $id . ".jpg";
        unlink($path);
        
        $query = sprintf("DELETE FROM announcements WHERE eventID=%d", (int)$id);
        $result = mysqli_query($link, $query);

        }


?>