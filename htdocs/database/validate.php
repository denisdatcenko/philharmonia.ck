<?php
 
include_once('connection.php');
  
function test_input($data) {
     
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}
  
if ($_SERVER["REQUEST_METHOD"] == "POST") {
     
    $username = test_input($_POST["username"]);
    $password = test_input($_POST["password"]);
    $users = array();
    $query = sprintf("SELECT * FROM admins");
    $link = db_connect();
    $result = mysqli_query($link, $query);

       if (!$result)
           die (mysqli_error($link));
        
    $n = mysqli_num_rows($result);
    $users = array();

        for ($i=0; $i<$n; $i++)
        {
            $row = mysqli_fetch_assoc($result);
            $users[] = $row;
        }
    
    foreach($users as $user) {
        
        if(($user['userLogin'] == $username) &&
            ($user['userPassword'] == $password)) {
                setcookie('login', $user['userLogin'], time() + (86400 * 30), "/");
                header("location: ../admin/admin_panel.php");
        }
        else {
            header("location: ../admin/index.php");
            die();
        }
    }
}
 
?>