<?php
    require_once ("check_cookies.php");
    require_once ("../database/connection.php");
    require_once ("../model/announcements.php");
    require_once ("../model/news.php");
    
    checkLogout();
    $link = db_connect();

    $announcements = announcements_all($link);
    $news = news_all($link);

    include("../views/admin_panel.php");

?>