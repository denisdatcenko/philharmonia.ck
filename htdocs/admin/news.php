<?php

    require_once("../database/connection.php");
    require_once("../model/news.php");
    
    $link = db_connect();

    if (isset($_GET['action']))
        $action = $_GET['action'];
    else
        $action = "";

    if ($action == "add"){
        if (!empty($_POST)){
            
            news_new($link, $_POST['newsTitle'], $_POST['newsDate'], $_POST['newsContent'], $_POST['newsPreview'], $_FILES['newsPhoto']);
            header("location: admin_panel.php");
        }
        include ("../admin/admin_panel.php");
    } 
        else {
    if ($action == "delete"){

        news_delete($link, $_GET['id']);
        header("location: admin_panel.php");
        include ("../admin/admin_panel.php");

    }
        else {
    if ($action == "edit"){
    
        $n = news_get($link, $_GET['id']);
        include ("../views/edit_news.php");

    }
        else {
    if ($action == "confirm_edit"){
        if (!empty($_POST)){
            
            news_edit($link, $_POST['newsID'], $_POST['newsTitle'], $_POST['newsDate'], $_POST['newsContent'], $_POST['newsPreview'], $_FILES['newsPhoto']);
            
            header("location: admin_panel.php");
        }
        include ("../admin/admin_panel.php");

    }
        else {
            $news = news_all($link);
            include("../views/new_news.php");
        }
        }
        }  
}   

?>