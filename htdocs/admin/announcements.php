<?php

    require_once("../database/connection.php");
    require_once("../model/announcements.php");
    
    $link = db_connect();

    if (isset($_GET['action']))
        $action = $_GET['action'];
    else
        $action = "";

    if ($action == "add"){
        if (!empty($_POST)){
            announcements_new($link, $_POST['eventName'], $_POST['eventDate'], $_POST['eventTime'], $_POST['eventDescription'], $_FILES['eventPoster']);
            header("location: admin_panel.php");
        }
        include ("../admin/admin_panel.php");
    } 
        else {
    if ($action == "delete"){

        announcements_delete($link, $_GET['id']);
        header("location: admin_panel.php");
        include ("../admin/admin_panel.php");

    }
        else {
    if ($action == "edit"){
    
        $e = announcements_get($link, $_GET['id']);
        include ("../views/edit_announcements.php");

    }
        else {
    if ($action == "confirm_edit"){
        if (!empty($_POST)){
            
            announcements_edit($link, $_POST['eventID'], $_POST['eventName'], $_POST['eventDate'], $_POST['eventTime'], $_POST['eventDescription'], $_FILES['eventPoster']);
            
            header("location: admin_panel.php");
        }
        include ("../admin/admin_panel.php");

    }
        else 
        $announcements = announcements_all($link);
        include("../views/new_announcement.php");
        }
        }  
}   

?>