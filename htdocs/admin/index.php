<html>
    <head>
        <title>Черкаська філармонія</title>
        <meta lang="ua">
        <link rel="stylesheet" href="../styles.css">
    </head>
    <body>
        <?php 
            include_once ("check_cookies.php");
            checkLogin();
        ?>
        
        <div class="login container">
            <div class="fw-clearfix">
                <h1>Вхід до панелі адміністратора</h1>
                <form сlass="login-form" action="../database/validate.php" method="post">
                    <div class="textbox">
                        <i class="fa fa-user" aria-hidden="true"></i>
                        <input type="text" placeholder="Ім'я користувача"
                             name="username" value="">
                    </div>
                    <div class="textbox">
                        <i class="fa fa-lock" aria-hidden="true"></i>
                        <input type="password" placeholder="Пароль"
                             name="password" value="">
                    </div>
                    <input class="button" type="submit"
                         name="login" value="Увійти">
                </form>
            </div>
        </div>
    </body>
</html>