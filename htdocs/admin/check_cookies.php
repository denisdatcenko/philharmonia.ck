<?php    

    function checkLogin() {
        if (isset($_COOKIE['login'])) {
            header ("location: ../admin/admin_panel.php");
        } 
    }

    function checkLogout() {
        if (!isset($_COOKIE['login'])) {
            header ("location: ../admin/index.php");
        } 
    }

   if (isset($_GET['action']))
        $action = $_GET['action'];
    else
        $action = "";

    if ($action == "logout") {
        setcookie('login', '', time()-60*15, '/');
        header ("location: ../admin/index.php");
    }

?>