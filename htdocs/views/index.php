<html>
    <head>
        <title>Черкаська філармонія</title>
        <meta lang="ua">
        <meta name="viewport" content="width=device-width,initial-scale=1.0">
        <link rel="stylesheet" href="styles.css">
    </head>
    <body>
                <div class="navbar container">
            <div class="fw-clearfix">
                <div class="logo">
                    <a href="index.php"><img class="logo-header" src="../img/main/logo-blc.png" alt="лого філармонії"></a>
                </div>
                <input type="checkbox" id="menu-input" class="menu-shower">
                <label id="responsive-menu" for="menu-input">
                    <div class="main-nav">
                        <a href="index.php">Головна</a>
                        <a href="news.php">Новини</a>
                        <a href="announcements.php">Афіша</a>
                    </div>
                </label>
            </div>
        </div>
        <div class="header container">
            <div class="fw-clearfix">
                <div class="banner banner-img" style="background-image: url('/img/main/banner_1.jpg');">
                    <div class="banner-content">
                        <h1>Головна музична сцена Черкащини</h1>
                        <div class="header-desc">
                            <p>Відкривайте для себе творчість митців з усіх куточків України та світу в стінах Черкаської філармонії! Запрошуємо до слідкування за афішами та новинами, аби не пропустити нові цікавинки зі світу музики й театру в Черкасах.</p>
                        </div>
                    </div>
                </div>
                <!--<div class="header-content">
                    <h1>Головна музична сцена Черкащини</h1>
                    <div class="header-desc w67">
                        <p>Відкривайте для себе творчість митців з усіх куточків України та світу в стінах Черкаської філармонії! Запрошуємо до слідкування за афішами та новинами, аби не пропустити нові цікавинки зі світу музики й театру в Черкасах.</p>
                    </div>
                </div>-->
            </div>
        </div>
        <div class="announcements container">
            <div class="fw-clearfix">
                <div class="clm-l w33">
                    <h2>Афіша</h2>
                </div>
                <div class="clm-l w67">
                    <div class="fw-clearfix">
                        <div class="announcements-grid">
                            <?php foreach ($announcements as $a): ?> 
                            <div class="announcements-grid-item">
                                <div class="announcements-poster preview-img" style="background-image: url(<?php echo $a['eventPosterPath']?>);"></div>
                                    
                                <p class="announcements-date"><?=date('d.m.Y <\b\r /> H:i', strtotime($a['eventDate']))?></p>
                                
                                <a class="announcements-title" href="event.php?id=<?=$a['eventID']?>&source=index"><?=$a['eventName']?></a>
                            </div>
                            <?php endforeach ?>
                            <div class="announcements-grid-item">
                                <a class="cta-link" href="announcements.php">Переглянути<br>всі анонси</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="news container">
            <div class="fw-clearfix">
                <h2>Останні новини</h2>
                <div class="news-grid">
                    <?php foreach ($news as $n): ?>
                    <div class="news-grid-item">
                        <div class="news-img preview-img" style="background-image: url('<?php echo $n['newsPhotoPath']?>');"></div>
                        <p class="news-date"><?=date('d.m.Y', strtotime($n['newsDate']))?></p>
                        <a class="news-title" href="newsitem.php?id=<?=$n['newsID']?>&source=index"><?=$n['newsTitle']?></a>
                    </div>
                    <?php endforeach ?>
                    
                    <div class="news-grid-item">
                        <a class="cta-link" href="news.php">Переглянути<br>всі новини</a>
                    </div>
                </div>
                
            </div>
        </div>
        <div class="footer container">
            <div class="fw-clearfix">
                <div class="clm-l w33">
                    <div class="logo">
                        <img class="logo-footer" src="/img/main/logo-wht.png" alt="лого філармонії">
                    </div>
                </div>
                <div class="clm-l w33">
                    <p class="about">
                        Черкаська обласна філармонія<br><br>
                        вул. Хрещатик 196 <br>
                        м. Черкаси, 18000 <br><br>
                        <a href="tel:+380472375493">+380 472 375 493</a> <br>
                        <a href="mailto:filarmonia.ua@gmail.com">filarmonia.ua@gmail.com</a>
                    </p>
                    <p class="credit">
                        Денис Даценко &copy; 2022
                    </p>
                </div>
                <div class="clm-l w33 bottom-text">
                    
                </div>
            </div>
        </div>
    </body>
</html>