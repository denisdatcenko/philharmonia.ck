<html>
    <head>
        <meta charset="utf-8">
        <title>Редагувати подію | Панель Адміністратора | Черкаська Філармонія</title>
        <link rel="stylesheet" href="../styles.css">
    </head>
    <body>
        <?php require_once('../admin/check_cookies.php'); checkLogout(); ?>
        <div class="admin-panel container">
            <div class="fw-clearfix">
                <h1>Редагування події</h1>
                <a class="cta-btn w33" href="../admin/admin_panel.php">Назад</a><br>
                <div class="form">
                    <form class="form" enctype="multipart/form-data" method="post" action="../admin/announcements.php?action=confirm_edit">
                        <input type="hidden" value="<?php echo $e['eventID'] ?>" name="eventID">
                        <label>
                            Назва події <br>
                            <?php echo '<input type="text" name="eventName" value="' . $e['eventName'] . '" class="form-item" autofocus required>';?>
                        </label>
                        <br>
                        <label>
                            Дата<br>
                            <?php echo '<input type="date" name="eventDate" value="'.date('Y-m-d', strtotime( $e['eventDate'])).'" class="form-item" required>';?>
                        </label>
                        <br>
                        <label>
                            Час<br>
                            <?php echo '<input type="time" name="eventTime" value="'.date('H:i', strtotime( $e['eventDate'])).'" class="form-item" required>';?>
                        </label>
                        <br>
                        <label>
                            Опис<br>
                            <textarea class="textarea form-item" name="eventDescription" maxlength="5000" required><?php echo $e['eventDescription'] ?></textarea>
                        </label>
                        <br>
                        <label>
                            Актуальний плакат:<br><br>
                            <div class="announcements-poster preview-img" style="background-image: url(<?php echo $e['eventPosterPath']?>);">
                            </div><br><br>
                            Новий файл:
                            <input type="file" name="eventPoster" accept=".jpg" class="form-item">
                        </label>
                        <br>
                        <input type="submit" value="Зберегти" class="btn">
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>