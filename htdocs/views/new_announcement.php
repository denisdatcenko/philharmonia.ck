<html>
    <head>
        <meta charset="utf-8">
		<meta name="viewport" content="width=device-width,initial-scale=1.0">
        <title>Додати подію | Панель Адміністратора | Черкаська Філармонія</title>
        <link rel="stylesheet" href="../styles.css">
    </head>
    <body>
        <?php require_once('../admin/check_cookies.php'); checkLogout(); ?>
        <div class="admin-panel container">
            <div class="fw-clearfix">
                <h1>Додати подію</h1>
                <a class="cta-btn w33" href="../admin/admin_panel.php">Назад</a><br>
                <form class="form" enctype="multipart/form-data" method="post" action="../admin/announcements.php?action=add">
                    <label>
                        Назва події <br>
                        <input type="text" name="eventName" value="" class="form-item" autofocus required>
                    </label>
                    <br>
                    <label>
                        Дата<br>
                        <input type="date" name="eventDate" value="" class="form-item" required>
                    </label>
                    <br>
                    <label>
                        Час<br>
                        <input type="time" name="eventTime" value="" class="form-item" required>
                    </label>
                    <br>
                    <label>
                        Опис<br>
                        <textarea class="textarea form-item" name="eventDescription" maxlength="5000" required></textarea>
                    </label>
                    <br>
                    <label>
                        Плакат<br>
                        <input type="file" name="eventPoster" accept=".jpg" class="form-item" required>
                    </label>
                    <br>
                    <input type="submit" value="Зберегти" class="btn">
                </form>
            </div>
        </div>
    </body>
</html>