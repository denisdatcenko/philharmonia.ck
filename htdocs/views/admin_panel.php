<html>
    <head>
        <meta charset="utf-8">
        <title>Панель адміністратора | Черкаська Філармнія</title>
        <link rel="stylesheet" href="../styles.css">
    </head>
    <body>
        <div class="admin-panel container">
            <div class="fw-clearfix">
                <h1>Панель адміністратора</h1>
                
                <form class="form" action="../admin/check_cookies.php?action=logout" method="post">
                    <input class="button" type="submit"
                         name="logout" value="Вийти">
                </form>
            </div>
        </div>
        <div class="admin-panel container">
            <div class="fw-clearfix">
                <h2>Афіші</h2>
                <a  class="cta-btn" href="announcements.php">
                    <p>Додати</p>
                </a>
                <table class="admin-table">
                    <tr>
                        <th>Дата</th>
                        <th>Назва події</th>
                        <th>Редагування</th>
                        <th>Видалення</th>
                    </tr>
                    <?php foreach($announcements as $a): ?>
                    <tr>
                        <td><?=$a['eventDate']?></td>
                        <td><?=$a['eventName']?></td>
                        <td><a href="announcements.php?action=edit&id=<?=$a['eventID']?>">Редагувати</a> </td>
                        <td><a href="announcements.php?action=delete&id=<?=$a['eventID']?>">Видалити</a> </td>
                    </tr>
                <?php endforeach ?>
                </table>
            </div>
        </div>
        <div class="admin-panel container">
            <div class="fw-clearfix">
                <h2>Новини</h2>
                <a  class="cta-btn" href="news.php">
                    <p>Додати</p>
                </a>
                <table class="admin-table">
                    <tr>
                        <th>Дата</th>
                        <th>Назва події</th>
                        <th>Редагування</th>
                        <th>Видалення</th>
                    </tr>
                    <?php foreach($news as $n): ?>
                    <tr>
                        <td><?=$n['newsDate']?></td>
                        <td><?=$n['newsTitle']?></td>
                        <td><a href="news.php?action=edit&id=<?=$n['newsID']?>">Редагувати</a> </td>
                        <td><a href="news.php?action=delete&id=<?=$n['newsID']?>">Видалити</a> </td>
                    </tr>
                <?php endforeach ?>
                </table>
            </div>
        </div>
    </body>
</html>