<html>
    <head>
        <title><?php echo $newsitem['newsTitle'] ?> | Черкаська філармонія</title>
        <meta lang="ua">
        <meta name="viewport" content="width=device-width,initial-scale=1.0">
        <link rel="stylesheet" href="styles.css">
    </head>
    <body>
              <div class="navbar container">
            <div class="fw-clearfix">
                <div class="logo">
                    <a href="index.php"><img class="logo-header" src="../img/main/logo-blc.png" alt="лого філармонії"></a>
                </div>
                <input type="checkbox" id="menu-input" class="menu-shower">
                <label id="responsive-menu" for="menu-input">
                    <div class="main-nav">
                        <a href="index.php">Головна</a>
                        <a href="news.php">Новини</a>
                        <a href="announcements.php">Афіша</a>
                    </div>
                </label>
            </div>
        </div>
        <div class="breadcrumb">
            <a class="breadcrumb-link" href="<?=$source?>">&#60 Назад</a></div>
        <div class="newsitem container">
            <div class="fw-clearfix">
                <div class="clm-l w33 newsitem-img">
                    <div class="news-img" style="background-image: url(<?php echo $newsitem['newsPhotoPath']?>);"></div>
                </div>
                <div class="clm-l w62 newsitem-info">
                    <h1 class="newsitem-title"><?=$newsitem['newsTitle']?></h1>
                    <p class="newsitem-date"><?=date('d.m.Y', strtotime($newsitem['newsDate']))?></p>
                    <p class="newsitem-content"><?=$newsitem['newsContent']?></p>
                </div>
            </div>
        </div>
        <div class="footer container">
            <div class="fw-clearfix">
                <div class="clm-l w33">
                    <div class="logo">
                        <img class="logo-footer" src="/img/main/logo-wht.png" alt="лого філармонії">
                    </div>
                </div>
                <div class="clm-l w33">
                    <p class="about">
                        Черкаська обласна філармонія<br><br>
                        вул. Хрещатик 196 <br>
                        м. Черкаси, 18000 <br><br>
                        <a href="tel:+380472375493">+380 472 375 493</a> <br>
                        <a href="mailto:filarmonia.ua@gmail.com">filarmonia.ua@gmail.com</a>
                    </p>
                    <p class="credit">
                        Денис Даценко &copy; 2022
                    </p>
                </div>
                <div class="clm-l w33 bottom-text">
                    
                </div>
            </div>
        </div>
    </body>
</html>