<html>
    <head>
        <meta charset="utf-8">
        <title>Редагувати новини | Панель Адміністратора | Черкаська Філармонія</title>
        <link rel="stylesheet" href="../styles.css">
    </head>
    <body>
        <?php require_once('../admin/check_cookies.php'); checkLogout(); ?>
        <div class="admin-panel container">
            <div class="fw-clearfix">
                <h1>Редагування новини</h1>
                <a class="cta-btn w33" href="../admin/admin_panel.php">Назад</a><br>
                <div class="form">
                    <form class="form" enctype="multipart/form-data" method="post" action="../admin/news.php?action=confirm_edit">
                        <input type="hidden" value="<?php echo $n['newsID'] ?>" name="newsID">
                        <label>
                            Заголовок<br>
                            <?php echo '<input type="text" name="newsTitle" value="' . $n['newsTitle'] . '" class="form-item" autofocus required>';?>
                        </label>
                        <br>
                        <label>
                            Дата<br>
                            <?php echo '<input type="date" name="newsDate" value="'.date('Y-m-d', strtotime( $n['newsDate'])).'" class="form-item" required>';?>
                        </label>
                        <br>
                        <label>
                            Текст <br>
                            <textarea class="textarea form-item" name="newsContent" maxlength="5000" required><?php echo $n['newsContent'] ?></textarea>
                        </label>
                        <br>
                        <label>
                            Короткий перегляд тексту<br>
                        <textarea class="textarea-short form-item" name="newsPreview" maxlength="250" required><?php echo $n['newsPreview'] ?></textarea>
                        </label>
                        <br>
                        <label>
                            Актуальнe фото:<br><br>
                            <div class="news-img preview-img" style="background-image: url(<?php echo $n['newsPhotoPath']?>);">
                            </div><br><br>
                            Новий файл:
                            <input type="file" name="newsPhoto" accept=".jpg" class="form-item">
                        </label>
                        <br>
                        <input type="submit" value="Зберегти" class="btn">
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>