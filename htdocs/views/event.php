<html>
    <head>
        <title><?php echo $event['eventName'] ?> | Черкаська філармонія</title>
        <meta lang="ua">
		<meta name="viewport" content="width=device-width,initial-scale=1.0">
        <link rel="stylesheet" href="styles.css">
    </head>
    <body>
        <div class="navbar container">
            <div class="fw-clearfix">
                <div class="logo">
                    <a href="index.php"><img class="logo-header" src="../img/main/logo-blc.png" alt="лого філармонії"></a>
                </div>
                <input type="checkbox" id="menu-input" class="menu-shower">
                <label id="responsive-menu" for="menu-input">
                    <div class="main-nav">
                        <a href="index.php">Головна</a>
                        <a href="news.php">Новини</a>
                        <a href="announcements.php">Афіша</a>
                    </div>
                </label>
            </div>
        </div>
        <!--<div class="header container">
            <div class="fw-clearfix">
                <div class="banner banner-img" style="background-image: url('/img/main/banner_3.jpg');"></div>
            </div>
        </div>-->
        <div class="breadcrumb">
            <a class="breadcrumb-link" href="<?=$source?>">&#60 Назад</a></div>
        <div class="event container">
            <div class="fw-clearfix">
                <div class="clm-l w33 event-poster">
                    <div class="announcements-poster" style="background-image: url(<?php echo $event['eventPosterPath']?>);"></div>
                </div>
                <div class="clm-l w62 event-info">
                    <h1 class="event-title"><?=$event['eventName']?></h1>
                    <p class="event-date"><?=date('d.m.Y <\b\r /> H:i', strtotime($event['eventDate']))?></p>
                    <p class="event-description"><?=$event['eventDescription']?></p>
                </div>
            </div>
        </div>
        <div class="footer container">
            <div class="fw-clearfix">
                <div class="clm-l w33">
                    <div class="logo">
                        <img class="logo-footer" src="/img/main/logo-wht.png" alt="лого філармонії">
                    </div>
                </div>
                <div class="clm-l w33">
                    <p class="about">
                        Черкаська обласна філармонія<br><br>
                        вул. Хрещатик 196 <br>
                        м. Черкаси, 18000 <br><br>
                        <a href="tel:+380472375493">+380 472 375 493</a> <br>
                        <a href="mailto:filarmonia.ua@gmail.com">filarmonia.ua@gmail.com</a>
                    </p>
                    <p class="credit">
                        Денис Даценко &copy; 2022
                    </p>
                </div>
                <div class="clm-l w33 bottom-text">
                    
                </div>
            </div>
        </div>
    </body>
</html>