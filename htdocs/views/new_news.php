<html>
    <head>
        <meta charset="utf-8">
        <title>Додати новину | Панель Адміністратора | Черкаська Філармонія</title>
        <link rel="stylesheet" href="../styles.css">
    </head>
    <body>
        <?php require_once('../admin/check_cookies.php'); checkLogout(); ?>
        <div class="admin-panel container">
            <div class="fw-clearfix">
                <h1>Додати новину</h1>
                <a class="cta-btn w33" href="../admin/admin_panel.php">Назад</a><br>
                <form class="form" enctype="multipart/form-data" method="post" action="../admin/news.php?action=add">
                    <label>
                        Заголовок <br>
                        <input type="text" name="newsTitle" value="" class="form-item" autofocus required>
                    </label>
                    <br>
                    <label>
                        Дата<br>
                        <input type="date" name="newsDate" value="<?php echo date('Y-m-d') ?>" class="form-item" required>
                    </label>
                    <br>
                    <label>
                        Текст <br>
                        <textarea class="textarea form-item" name="newsContent" maxlength="5000" required></textarea>
                    </label>
                    <br>
                    <label>
                        Короткий перегляд тексту<br>
                        <textarea class="textarea-short form-item" name="newsPreview" maxlength="250" required></textarea>
                    </label>
                    <br>
                    <label>
                        Фото<br>
                        <input type="file" name="newsPhoto" accept=".jpg" class="form-item" required>
                    </label>
                    <br>
                    <input type="submit" value="Зберегти" class="btn">
                </form>
            </div>
        </div>
    </body>
</html>