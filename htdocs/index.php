<?php 
    require_once ("database/connection.php");
    require_once ("model/announcements.php");
    require_once ("model/news.php");
    
    $link = db_connect();

    $announcements = announcements_upcoming($link);
    $news = news_recent($link);
        
    include("views/index.php");

?>