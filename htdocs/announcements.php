<?php 
    require_once ("database/connection.php");
    require_once ("model/announcements.php");
    
    $link = db_connect();

    $announcements = announcements_all($link);
        
    include("kursova/views/announcements.php");

?>