-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1
-- Час створення: Гру 13 2022 р., 20:02
-- Версія сервера: 10.4.27-MariaDB
-- Версія PHP: 8.1.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База даних: `philharmonic`
--

-- --------------------------------------------------------

--
-- Структура таблиці `admins`
--

CREATE TABLE `admins` (
  `userID` int(11) NOT NULL,
  `userLogin` varchar(16) NOT NULL,
  `userPassword` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Дамп даних таблиці `admins`
--

INSERT INTO `admins` (`userID`, `userLogin`, `userPassword`) VALUES
(1, 'adminName', 'Pass');

-- --------------------------------------------------------

--
-- Структура таблиці `announcements`
--

CREATE TABLE `announcements` (
  `eventID` int(11) NOT NULL,
  `eventDate` datetime DEFAULT NULL,
  `eventName` varchar(255) DEFAULT NULL,
  `eventDescription` varchar(5000) DEFAULT NULL,
  `eventPosterPath` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Дамп даних таблиці `announcements`
--

INSERT INTO `announcements` (`eventID`, `eventDate`, `eventName`, `eventDescription`, `eventPosterPath`) VALUES
(25, '2022-12-24 17:00:00', 'Різдвяний концерт — Академічний Симфонічний Оркестр', 'Незабутній  вечір у Черкаській обласній філармонії як особливий музичний подарунок у дусі Різдва 2022\r\nРіздвяно-новорічні дні - це завжди особливий час; час нових надій і особливих подарунків....\r\nНасолодитися особливим  музичним подарунком - різдвяним  концертом академічного симфонічного оркестру під орудою заслуженого діяча мистецтв України Олександра Дяченка \r\nзапрошуємо 24 та 25 грудня о 17.00 у залі Черкаської обласної філармонії.\r\nМузика , що по особливому торкає серця, - то  дар, якого  прагне світла душа і в різдвяно-новорічні календарні свята, створюючи своєю магією мажорний настрій   надії та  впевненості у нашому  майбутньому.\r\n Музичні  перлини від академічного симфонічного оркестру цього  концертного вечора наповнять магічним скарбом кожне биття серця та подарують святковий настрій із сподіваннями  благословенного миру для України та світу! \r\nMerry coming Christmas!!!', '/img/ann_25.jpg'),
(26, '2022-12-25 17:00:00', 'Різдвяний концерт — Академічний Симфонічний Оркестр', 'Незабутній  вечір у Черкаській обласній філармонії як особливий музичний подарунок у дусі Різдва 2022\r\nРіздвяно-новорічні дні - це завжди особливий час; час нових надій і особливих подарунків....\r\nНасолодитися особливим  музичним подарунком - різдвяним  концертом академічного симфонічного оркестру під орудою заслуженого діяча мистецтв України Олександра Дяченка \r\nзапрошуємо 24 та 25 грудня о 17.00 у залі Черкаської обласної філармонії.\r\nМузика , що по особливому торкає серця, - то  дар, якого  прагне світла душа і в різдвяно-новорічні календарні свята, створюючи своєю магією мажорний настрій   надії та  впевненості у нашому  майбутньому.\r\n Музичні  перлини від академічного симфонічного оркестру цього  концертного вечора наповнять магічним скарбом кожне биття серця та подарують святковий настрій із сподіваннями  благословенного миру для України та світу! \r\nMerry coming Christmas!!!', '/img/ann_26.jpg'),
(27, '2022-12-27 18:00:00', 'Український вертеп', 'Відчути справжній український дух зимових  традицій, поринувши  в захопливий екскурс пісенно-музичної  історії свого народу, запрошуємо на театралізованому дійстві \"Український вертеп\" у Черкаській обласній філармонії....\r\nА ще цілий міх захопливих сюрпризів та приємних несподіванок вже чекають на вас наприкінці грудня!\r\nГостинно запрошуємо стати свідками театралізованого дійства від Черкаського народного хору!!!\r\nТож, Черкаська обласна філармонія 27 грудня о 18:00 гостинно запрошує на продовження святкування Різдва Христового, на коляду та багату кутю..., бо ж саме різдвяно-новорічні дні - це час, що переповнений багатством українських  народних традицій та звичаїв.  Грудневого концертного вечора любителів українських різдвянок зачарує колоритна постановка українського вертепу та глибоке звучання традиційних різдвяних пісень та щедрівок у виконанні лауреата Національної премії України ім. Т. Г. Шевченка - Черкаського академічного заслуженого українського народного хору!\r\nПереможно впустіть добру звістку й щиру віру, надію  і світло великої  любові  у свою душу!\r\nЧекаємо на Вас всією родиною!', '/img/ann_27.jpg'),
(28, '2022-12-19 18:00:00', 'ОБЕРЕЖНО ГАРЯЧЕ!', '19.12.2022 року в залі Черкаської обласної філармонії відбуватиметься по справжньому гаряча подія – перший сольний концерт молодого гурту – тріо «ОБЕРЕЖНО ГАРЯЧЕ!» - Василь Клименко, Вячеслав Дяченко та Віталій Панченко.\r\n\r\nУ програмі концерту прозвучать музичні композиції, які перенесуть вас у світ справжнього музичного задоволення, змушуватимуть затамовувати подих і співати разом з молодими артистами!\r\n\r\nУ концерті братимуть участь спеціальні гості – stage bаnd «Paraleli» під керівництвом Олександра Лісуна та солістка Кароліна Перевізник.\r\n\r\nЦе концерт для нескорених духом українців, який наповнить натхненням, додасть сил, віри і святкового настрою кожному глядачу.\r\n\r\nНе пропустіть цю подію! Відсвяткуємо День Св. Миколая разом з тріо «ОБЕРЕЖНО ГАРЯЧЕ!».\r\n\r\nЧекаємо на вас з нетерпінням!\r\n\r\nЖивий звук.', '/img/ann_28.jpg');

-- --------------------------------------------------------

--
-- Структура таблиці `news`
--

CREATE TABLE `news` (
  `newsID` int(11) NOT NULL,
  `newsDate` datetime DEFAULT NULL,
  `newsTitle` varchar(255) DEFAULT NULL,
  `newsContent` varchar(5000) DEFAULT NULL,
  `newsPreview` varchar(250) DEFAULT NULL,
  `newsPhotoPath` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Дамп даних таблиці `news`
--

INSERT INTO `news` (`newsID`, `newsDate`, `newsTitle`, `newsContent`, `newsPreview`, `newsPhotoPath`) VALUES
(3, '2022-12-06 00:00:00', 'Як у Черкаській Філармонії гомоніли музи', 'Ознаменувавши початок періоду українських свят, що за календарем,  в реаліях сьогодення  6 грудня стало надзвичайно важливою  датою для України, що вкотре привертає увагу до сильних духом сміливців та  відзначення їх особливого дня -  Дня Збройних Сил України. Тож, зустріч із музикою у концерті “”Щоб музи не мовчали” Академічного симфонічного оркестру під диригуванням заслуженого діяча мистецтв України Олександра Дяченка у  Черкаській обласній філармонії увібрала в собі яскраву різнопланову програму. Цього особливого концертного вечора прозвучала музика відомих творів 19 століття та нові композиції, що стали справжнім подарунком для  прихильників класичної і сучасної музики та вишуканих меломанів, в серцях яких резонують звуки свободи і мелодії великої любові!\r\nТалант і натхненна праця диригента Олександра  Дяченка підкорили різнохарактерні контрапункти симфонічних мініатюр живильним бальзамом на воскресіння інтерпретації музики у перехресті сучасних подій. Прекрасні солісти з міста-героя  Києва - лауреати міжнародних конкурсів: Роман Фотуйма (саксофон) та Дар`я Шутко (фортепіано) презентували нову сучасну музику молодого українського композитора, котрий родом з Черкащини, - Сергія Леонтьєва. Їх виконання продемонструвало високу майстерність та професіоналізм у грі технічних сольних партій з яскравою ритмікою та харизматичними мелодіями. Музика талановитого композитора Сергія Леонтьєва зачарувала витонченою майстерністю  наскрізного розвитку у вишуканій манері виконання артистів оркестру та солістів!\r\nКонцентрична побудова програми в середині містила “Danse macabre” \r\nК.Сен-Санса як пророчий передзвін-нагадування про силу, сенс та цінність життя у чудовому виконанні соліста - заслуженого артиста України Геннадія Кабіна (скрипка).\r\nЗлагоджений стрій симфонічного оркестру під орудою блискучого диригента Олександра Дяченка створив одну велику подію - історичну сповідь із яскравими композиціями 19 століття: Каміля Сен-Санса, Жуля Массне, барона Еміля фон Резнічека та  колоритними симфонічними мініатюрами 20 століття: Найджела  Хесса,  Ніло Менендеса, Жана Матітіа та знаковим для українського народу  “Запорізьким маршем” Євгена Адамцевича в присвяту усім поколінням  Захисників України. \r\n Надзвичайно проникливий музикознавчий супровід дарувала слухачам ведуча концерту Тетяна Гук, а відеоряд відеорежисера Руслана Павлюка  додав особливої фарби концертній звуковій палітрі. \r\nЗавдячуючи Збройним Силам України, які боронять нас, аби ми музикували, яскравим фінальним акордом  концерту прозвучав Гімн України у спільному величному звучанні Академічного симфонічного оркестру  та щиросердному співі  повної глядацької  зали Черкаської обласної  філармонії! \r\nЧи це не є ознакою єдності та згуртованості української нації всіма силами рухатися до своєї Перемоги! \r\nПеремозі сильної духом і талановитої України вже незабаром бути!!!', 'Ознаменувавши початок періоду українських свят, що за календарем,  в реаліях сьогодення  6 грудня стало надзвичайно важливою  датою для України, що вкотре привертає увагу до сильних духом сміливців та  відзначення їх особливого дня...', '/img/news_3.jpg'),
(4, '2022-12-04 00:00:00', 'Відзначаємо 300-ліття Григорія Сковороди', 'Зимну пору , що багата на календарні  свята,  в Черкаській обласній філармонії розпочав концерт, приурочений до дня народження геніального у своїй думці українського мислителя, педагога , поета, співця та музиканта, - Григорія Сковороди. Саме цього нелегкого, але вирішального для України і українців року ми відзначаємо 300-ліття від дня народження філософа українського духу і серця, що у свій час, виховуючи сина Василя Томари у селі Коврай у нас на Черкащині, музикував і писав свою поезію, нашого українського Сократа Григорія Савича Сковороди. Саме його думки відразу розліталися, розносилися лірниками і співцями серед народу і значною мірою вплинули на національну свідомість сучасників й всіх подальших поколінь. \r\nТворчі колективи Черкаської обласної філармонії також підтримали ініціативу вдячності великому українському мислелюбу, а тому 3 грудня світло великої любові й мудрості Григорія Сковороди яскраво осяяло  надзвичайно прекрасний у своїй красі концерт під звучною назвою «Любов виникає з любові», режисеркою -постановницею якого була Галина Федоряка. Сама назва концерту, який провела народна артистка України Ольга Калина, відразу повернула до  творчої спадщини філософа і вкотре спонукає замислитися над її змістом.\r\nХолодного  суботнього дня талановиті артисти філармонійної мистецької родини зігрівали звучанням своїх голосів кожну гостинну душу, яка завітала на вшанування культурної спадщини Григорія СКОВОРОДИ в музичнім обрамленні творів українських композиторів - ювілярів: Миколи ЛИСЕНКА та Кирила СТЕЦЕНКА - неповторних українських діячів мистецтва та культури. \r\n На творчих стежках митців-ювілярів, на потіху сердець глядачів, знайшли пісенні скарби: Черкаський академічний заслужений український народний хор ( художній керівник та головний диригент Володимир Федас; хормейстер Євген Пихтін; балетмейстерка-постановниця Ірина Ворошилова; керівник оркестру заслужений артист України Олексій Колесник) з вокально-хореографічною композицією \"Роде наш красний\",  молитвами: \"Благослови, душе моя, Господа\" та \"Богородице Діво, радуйся\"\r\n (К. Стеценко), \"Ой, на горі сніг біленький\", солістка Яна Видиборець (В. Стеценко);\r\nансамбль \"Росава\", художній керівник заслужений артист України Володимир Локтєв з композиціями: «Вечірня пісня», солістки Аліна Вікарчук, Вероніка Косенко ( К. Стеценко), «Весна люба, ах прийшла», солістка Катерина Овсієнко ( Г. Сковорода), «Де з Господом», \" Ой, ходила дівчина\", солістка Галина Боровицька, Катерина Овсієнко ( Г. Сковорода, М. Лисенко), \r\n«Ой не світи,  місяченьку», солістка Катерина Овсієнко ( М. Лисенко) та інш.\r\n В’ячеслав Дяченко виконав всім добре відомий твір - «Всякому городу нрав і права», а заслужений артист України Юрій Ільчук  - «Ой, ти птичко жовтобока»!\r\nГлядачі у залі  неймовірно палко дякували за самобутній спів та гру на бандурі  і співцю українського народу Євгену Пихтіну, а також -  і за постановку Г. Клокова «Удовиця» (хореографія В. Чунихина) у виконанні  Ірини й Віталія  Ворошилових та заслуженого артиста України  Андрія Єфімова, а ще  за хореографічну композицію \"Паняночка\" від  \r\nВ. Білошкурського у виконанні балетної групи Черкаського народного хору,  бо ж\r\n\"Всяк должен узнать свій народ і себе в ньому\" за Григорієм  Сковородою.\r\n В очікуванні такої бажаної нам сьогодні   перемоги    усі разом не забуваймо про видатних українців, котрі і сотні літ тому  творили історію нашої могутньої української держави. Тримаймося, єднаймося  і разом творімо краще світле майбутнє України!', 'Зимну пору , що багата на календарні  свята,  в Черкаській обласній філармонії розпочав концерт, приурочений до дня народження геніального у своїй думці українського мислителя, педагога, поета, співця та музиканта, - Григорія Сковороди.', '/img/news_4.jpg'),
(5, '2022-12-01 00:00:00', 'Італійський музичний вечір у Черкасах', 'Довгі лінії музичних фраз, мелодійні пасажі, вражаюче володіння голосом і відточена краса віртуозного співу - ці слова беззаперечно точно описують події вечірної казки вокальної музики , яка відбулася останнього осіннього концертного вечора  в залі Черкаської обласної філармонії під музичною назвою «Краса belcanto»! \r\nУ цьому концерті переважали композиції в італійському стилі BELCANTO! Незважаючи на плавність і співучість, арії, що виконуються у такій манері, звучать завжди надзвичайно виразно і граціозно!\r\nМузичного вечора  поціновувачі високого мистецтва італійської витонченої музики мали шедевральну можливість насолоджуватися творами відомих італійських композиторів Гаетано Доніцетті, Вінченцо Белліні та Гаспаре Спонтіні та інш.    Лауреатки міжнародних конкурсів : Олена Лютаревич-Марченко\r\n(сопрано) та Олеся Рожкова (сопрано) у супроводі роялю (концермейстер -заслужена артистка України - Олена Бєлкіна)  наповнювали душі красою виконання та любов’ю, змушуючи пробудитися, здавалося б, давно зниклі почуття…\r\nВедуча вокального вечора - народна артистка  України  Ольга  Калина додала витонченого словесного шарму.  Голоси і чарівна виконавська краса BELCANTO, цього вечора розливалися повноводно , зачаровуючи серця музичних меломанів, що сповна насолодилися !!!\r\nBravo! Bravissimo!!!', 'Довгі лінії музичних фраз, мелодійні пасажі, вражаюче володіння голосом і відточена краса віртуозного співу - ці слова беззаперечно точно описують події вечірної казки вокальної музики, яка відбулася останнього осіннього концертного вечора...', '/img/news_5.jpg'),
(6, '2022-11-25 00:00:00', 'В обіймах осені', 'Красива музика надихає увесь світ, дарує душі крила і  додає життя…  \r\nКоли десь далеко у світі відзначався День Подяки, в честь музичного мистецтва  дощового листопадового  вечора у вишуканому залі Черкаської обласної філармонії відбулася особлива музична подія! Пройшов неосяжної краси музичний хвилюючий вечір, котрий отримав назву \r\n«В обіймах осені». Музика, яка прозвучала цього вечора розкривала й очищувала душу і подарувала кожному краплинку світла, радості, любові, надії  й ще більше  краси та гармонії. \r\nМузичний  гурт STAGE BAND «Паралелі» під художньою орудою Олександра Лісуна запалив вогники в очах і вдячних  серцях всіх, хто завітав до Черкаської обласної філармонії.\r\n Кароліна Перевізник, Олександр Москаленко, Галина Боровицька, Вероніка Косенко, Інна Костюніна, Юрій Сокур  та тріо солістів «Обережно – гаряче»: Василь Клименко, Віталій Панченко та В\'ячеслав Дяченко талановито  оздобили своєю вокально-інструментальною красою осінній музичний  вечір наприкінці  листопада.Тож,  у концертній програмі від Stage band \r\n\" Паралелі\" та молодих виконавців звучали  композиції: “Відлуння вірності”(Галина Боровицька), “Лебедина вірність”( Олександр Москаленко), “Жовтий лист” (Василь Клименко у супроводі хореографії Ірини Ворошилової та заслуженого артиста України Андрія Єфімова), “Broken Vow” (Інна Костюніна), “There You’ll Be”, “Me Equlvoque” (Кароліна Перевізник та балетна пара: Андрій Єфімов , Анна Косенко), “Почуття\" ( Василь Клименко), “Quando, Quando, Quando” (Василь Клименко та Кароліна Перевізник), “Don\'t you Worry \' Bout A Thing”, “Tico, Tico” ( Юрій Сокур), “Чи це любов?” (Василь Клименко та балетна група), “Стожари\"  (Вячеслав Дяченко) і звичайно , \r\n\" Червона рута\" ( Василь Клименко, Віталій Панченко, В\'ячеслав Дяченко) та інш.  Під заворожуючі виконання артистів, у сяйві музики,  що надихала, та конферанс  ведучого Юрія Федоряки глядацьким залом  невпинно котилася хвиля оплесків, позитивних емоцій і вигуків “bravo”.\r\nДосконала музика, що виконувалась і що прожита серцем, то, безсумнівно, -  яскраве щастя. Теплий вечір, режисером-сценаристом якого була Галина Федоряка, попри всі виклики часу,  не залишив без приємних емоцій жодного поціновувача прекрасного музичного мистецтва!\r\n У цей непростий час музика, що йде від серця до серця, об‘єднує, дає сили та наснаги жити! \r\nВсе буде Україна!', 'Красива музика надихає увесь світ, дарує душі крила і  додає життя…  \r\nКоли десь далеко у світі відзначався День Подяки, в честь музичного мистецтва  дощового листопадового  вечора у вишуканому залі Черкаської обласної філармонії відбулася особлива..', '/img/news_6.jpg');

--
-- Індекси збережених таблиць
--

--
-- Індекси таблиці `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`userID`);

--
-- Індекси таблиці `announcements`
--
ALTER TABLE `announcements`
  ADD PRIMARY KEY (`eventID`);

--
-- Індекси таблиці `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`newsID`);

--
-- AUTO_INCREMENT для збережених таблиць
--

--
-- AUTO_INCREMENT для таблиці `admins`
--
ALTER TABLE `admins`
  MODIFY `userID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблиці `announcements`
--
ALTER TABLE `announcements`
  MODIFY `eventID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT для таблиці `news`
--
ALTER TABLE `news`
  MODIFY `newsID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
